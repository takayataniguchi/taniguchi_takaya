package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> select(Connection connection, Date startDate, Date endDate, String category) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT");
			sql.append("	messages.message_id as id,");
			sql.append("	users.name as name,");
			sql.append("	messages.user_id as user_id,");
			sql.append("	messages.title as title,");
			sql.append("	messages.category as category,");
			sql.append("	messages.text as text,");
			sql.append("	messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.user_id ");
			sql.append("WHERE messages.created_date ");
			sql.append("BETWEEN ? ");
			sql.append("AND ? ");
			if (category != null) {
				sql.append("AND messages.category LIKE ?");
			}
			sql.append("ORDER BY created_date DESC");
			sql.append(";");

			ps = connection.prepareStatement(sql.toString());

			ps.setObject(1, startDate, Types.TIMESTAMP);
			ps.setObject(2, endDate, Types.TIMESTAMP);
			if (category != null) {
				ps.setString(3, "%" + category + "%");
			}

			ResultSet rs = ps.executeQuery();

			List<UserMessage> messages = toUserMessages(rs);

			return messages ;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

		List<UserMessage> messages = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				UserMessage message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setName(rs.getString("name"));
				message.setUserId(rs.getInt("user_id"));
				message.setTitle(rs.getString("title"));
				message.setCategory(rs.getString("category"));
				message.setText(rs.getString("text"));
				message.setCreatedDate(rs.getTimestamp("created_date"));

				messages.add(message);
			}
			return messages;
		} finally {
			close(rs);
		}
	}
}
