package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("	users.user_id as user_id, ");
			sql.append("	users.account as account, ");
			sql.append("	users.name as name, ");
			sql.append("	users.is_stopped as is_stopped, ");
			sql.append("	users.created_date as created_date, ");
			sql.append("	users.updated_date as updated_date, ");
			sql.append("	branches.branch_id as branch_id, ");
			sql.append("	branches.name as branch, ");
			sql.append("	departments.department_id as department_id, ");
			sql.append("	departments.name as department ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.branch_id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.department_id ");
			sql.append("ORDER BY users.created_date ASC ");
			sql.append(";");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> user = toUsers(rs);
			return user;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchDepartment> toUsers(ResultSet rs) throws SQLException {

		List<UserBranchDepartment> users = new ArrayList<UserBranchDepartment>();
		try {
			while (rs.next()) {
				UserBranchDepartment user = new UserBranchDepartment();
				user.setUserId(rs.getInt("user_id"));
				user.setAccount(rs.getString("account"));
				user.setName(rs.getString("name"));
				user.setBranchName(rs.getString("branch"));
				user.setDepartmentName(rs.getString("department"));
				user.setIsStoped(rs.getInt("is_stopped"));
				user.setCreatedDate(rs.getTimestamp("created_date"));
				user.setCreatedDate(rs.getTimestamp("updated_date"));

				users.add(user);
			}
			return users;
		} finally {
			close(rs);
		}
	}

}
