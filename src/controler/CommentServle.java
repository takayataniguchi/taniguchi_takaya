package controler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = {"/comment"})
public class CommentServle extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		String writenComment = request.getParameter("comment");

		HttpSession session = request.getSession();

		if (!isValid(errorMessages, writenComment)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		Comment comment = new Comment();
		comment.setText(writenComment);

		String messageId = request.getParameter("messageId");
		comment.setMessageId(Integer.parseInt(messageId));

		User user = (User) session.getAttribute("loginUser");
		comment.setUserId(user.getUserId());

		new CommentService().insert(comment);
		response.sendRedirect("./");
	}

	private boolean isValid (List<String> errorMessages, String writenComment) {

		String checkWritenComment = writenComment.replaceAll("　", "").replaceAll(" ", "");

		if (StringUtils.isEmpty(checkWritenComment)) {
			errorMessages.add("コメントを入力してください");
		} else if (!(500 >= writenComment.length())) {
			errorMessages.add("コメントは500文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return  false;
		}

		return true;

	}
}