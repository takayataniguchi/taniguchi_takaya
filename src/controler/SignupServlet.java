package controler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;

//import org.apache.commons.lang.StringUtils;

import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String confirmationPassword = request.getParameter("confirmationPassword");
		String name = request.getParameter("name");
		Integer branchId = Integer.parseInt(request.getParameter("branchId"));
		Integer departmentId = Integer.parseInt(request.getParameter("departmentId"));

		User user = new User();
		user.setAccount(account);
		user.setPassword(password);
		user.setConfirmationPassword(confirmationPassword);
		user.setName(name);
		user.setBranchId(branchId);
		user.setDepartmentId(departmentId);

		if (!isValid (errorMessages, account, password, confirmationPassword, name, branchId, departmentId)) {
			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();
			request.setAttribute("user", user);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}

		User accountDuplicate = new UserService().select(account);

		if (accountDuplicate != null) {
			errorMessages.add("アカウントが重複しています");
			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();
			request.setAttribute("user", user);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}

		new UserService().insert(user);
		response.sendRedirect("./management");
	}

	private boolean isValid (List<String> errorMessages, String account, String password,
			String confirmationPassword, String name, Integer branchId, Integer departmentId) {

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウントを入力してください");
		} else if (!(account.length() <= 20 && account.matches("^[a-zA-Z0-9]+$"))) {
			errorMessages.add("アカウントは20文字以下で入力してください");
		} else if (!(account.length() >= 6 && account.matches("^[a-zA-Z0-9]+$"))) {
			errorMessages.add("アカウントは6文字以上で入力してください");
		}

		if (StringUtils.isBlank(password) && StringUtils.isBlank(confirmationPassword)) {
			errorMessages.add("パスワードを入力してください");
		} else if (!(password.length() >= 6 && password.matches("^[¥x20-¥x7F]+$"))) {
			errorMessages.add("パスワードは6文字以上で入力してください");
		} else if (!(password.length() <= 20 && password.matches("^[¥x20-¥x7F]+$"))) {
			errorMessages.add("パスワードは20文字以下で入力してください");
		}

		if (!password.equals(confirmationPassword)) {
			errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
		}

		if (StringUtils.isBlank(name)) {
			errorMessages.add("ユーザー名を入力してください");
		}

		if (10 < name.length()) {
			errorMessages.add("ユーザー名は10文字以下で入力してください");
		}

		if (branchId == 1) {
			if (!(departmentId <= 2)) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}
		}

		if (branchId != 1) {
			if (!(departmentId >= 3)) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}
		}

		if (errorMessages.size() != 0) {
			return false;
		}

		return true;
	}
}