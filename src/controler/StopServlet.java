package controler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {

		request.getRequestDispatcher("./management").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int switchedStatusUserId = Integer.parseInt(request.getParameter("userId"));
		int isStopped = Integer.parseInt(request.getParameter("isStoped"));

		new UserService().switchStatus(switchedStatusUserId, isStopped);

		response.sendRedirect("./management");
	}
}
