package controler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int userId = Integer.parseInt(request.getParameter("userId"));

		User editUser = new UserService().select(userId);
		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		request.setAttribute("editUser", editUser);
		request.setAttribute("editDisplayBranches", branches);
		request.setAttribute("editDisplayDepartments", departments);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();

		int userId = Integer.parseInt(request.getParameter("userId"));
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String confirmationPassword = request.getParameter("confirmationPassword");
		String name = request.getParameter("name");
		Integer branchId = Integer.parseInt(request.getParameter("branchId"));
		Integer departmentId = Integer.parseInt(request.getParameter("departmentId"));

		User editUser = new User();
		editUser.setUserId(userId);
		editUser.setAccount(account);
		editUser.setPassword(password);
		editUser.setConfirmationPassword(confirmationPassword);
		editUser.setName(name);
		editUser.setBranchId(branchId);
		editUser.setDepartmentId(departmentId);

		if (!isValid(errorMessages, account, password, confirmationPassword, name, branchId, departmentId)) {
			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();
			request.setAttribute("editUser", editUser);
			request.setAttribute("editDisplayBranches", branches);
			request.setAttribute("editDisplayDepartments", departments);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		User accountDuplicate = new UserService().select(account);


		if (accountDuplicate != null) {

			int userIdDuplicate = accountDuplicate.getUserId();
			int editUserId = Integer.parseInt(request.getParameter("userId"));

			if (userIdDuplicate != editUserId) {
				errorMessages.add("アカウントが重複しています");
				List<Branch> branches = new BranchService().select();
				List<Department> departments = new DepartmentService().select();
				request.setAttribute("editUser", editUser);
				request.setAttribute("editDisplayBranches", branches);
				request.setAttribute("editDisplayDepartments", departments);
				request.setAttribute("errorMessages", errorMessages);
				request.getRequestDispatcher("setting.jsp").forward(request, response);
				return;
			}
		}

		try {
			new UserService().update(editUser);
		} catch (NoRowsUpdatedRuntimeException e) {
			errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();
			request.setAttribute("editUser", editUser);
			request.setAttribute("editDisplayBranches", branches);
			request.setAttribute("editDisplayDepartments", departments);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		response.sendRedirect("./management");
	}

	private boolean isValid (List<String> errorMessages, String account, String password,
			String confirmationPassword, String name, Integer branchId, Integer departmentId) {

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウントを入力してください");
		} else if (!(account.length() <= 20 && account.matches("^[a-zA-Z0-9]+$"))) {
			errorMessages.add("アカウントは20文字以下で入力してください");
		} else if (!(account.length() >= 6 && account.matches("^[a-zA-Z0-9]+$"))) {
			errorMessages.add("アカウントは6文字以上で入力してください");
		}

		if (!(StringUtils.isBlank(password) && StringUtils.isBlank(confirmationPassword))) {
			if (!(password.length() >= 6 && password.matches("^[¥x20-¥x7F]+$"))) {
				errorMessages.add("パスワードは6文字以上で入力してください");
			} else if (!(password.length() <= 20 && password.matches("^[¥x20-¥x7F]+$"))) {
				errorMessages.add("パスワードは20文字以下で入力してください");
			}
		}

		if (!password.equals(confirmationPassword)) {
			errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
		}

		if (StringUtils.isBlank(name)) {
			errorMessages.add("ユーザー名を入力してください");
		}

		if (10 < name.length()) {
			errorMessages.add("ユーザー名は10文字以下で入力してください");
		}

		if (branchId == 1) {
			if (!(departmentId <= 2)) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}
		}

		if (branchId != 1) {
			if (!(departmentId >= 3)) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}
		}

		if (errorMessages.size() != 0) {
			return false;
		}

		return true;


	}
}