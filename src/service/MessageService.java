package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

		public void insert(Message message) {

			Connection connection = null;
			try {
				connection = getConnection();

				new MessageDao().insert(connection, message);
				commit(connection);
			} catch (RuntimeException e) {
				rollback(connection);
				throw e;
			} catch (Error e) {
				rollback(connection);
				throw e;
			} finally {
				close(connection);
			}
		}

		public List<UserMessage> select(String filteredStartDate, String filteredEndDate, String category) {

			Date startDate = new Date();
			if (!(filteredStartDate == null || filteredStartDate == "")) {
				filteredStartDate = filteredStartDate + " 00:00:00";
			} else {
				filteredStartDate = "2020/01/01 00:00:00";
			}

			try {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
				startDate = format.parse(filteredStartDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Date endDate = new Date();
			if (!(filteredEndDate == null || filteredEndDate == "")) {
				filteredEndDate = filteredEndDate + " 23:59:59";

				try {
					SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
					endDate = format.parse(filteredEndDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			Connection connection = null;

			try {
				connection = getConnection();

				List<UserMessage> messages = new UserMessageDao().select(connection, startDate, endDate, category);

				commit(connection);
				return messages;
			} catch (RuntimeException e) {
				rollback(connection);
				throw e;
			} catch (Error e) {
				rollback(connection);
				throw e;
			} finally {
				close(connection);
			}
		}

		public void delete(int messageId) {

			Connection connection = null;
			try {
				connection = getConnection();
				new MessageDao().delete(connection, messageId);
				commit(connection);

			} catch (RuntimeException e) {
				rollback(connection);
				throw e;
			} catch (Error e) {
				rollback(connection);
				throw e;
			} finally {
				close(connection);
			}
		}
}
