package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns= {"/*"})
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		User user = (User) session.getAttribute("loginUser");

		String errorMessage = "ログインしてください";

		if(user == null && (!(((HttpServletRequest)request).getServletPath()).contains("login"))){
			session.setAttribute("errorMessages", errorMessage);
			httpResponse.sendRedirect("./login");
			return;
		}else {
			chain.doFilter(request, response);
		}

	}

	public void init(FilterConfig fConfig) throws ServletException {

	}

	public void destroy() {

	}
}