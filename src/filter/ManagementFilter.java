package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns= {"/signup", "/setting", "/management"})
public class ManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		User user = (User) session.getAttribute("loginUser");
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		List<String> errorMessages = new ArrayList<String>();

		if (!(branchId == 1 && departmentId == 1)) {
			errorMessages.add("権限がありません");
			session.setAttribute("errorMessages", errorMessages);
			httpResponse.sendRedirect("./");
			return;
		}

		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {

	}

	public void destroy() {

	}
}