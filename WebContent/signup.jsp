<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー新規登録</title>
	</head>
	<body>
		<div class="main-contents">

			<a href= "management" class="link">ユーザー管理</a><br /> <br />
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }"></c:out>
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<form action="signup" method="post">
				<label for="account">アカウント(半角英数字の6文字以上20文字以下)</label><br />
				<input name="account" value="${user.account}" /><br />

				<label for="password">パスワード(記号を含む全ての半角文字で6文字以上20文字以下)</label><br />
				<input name="password" type="password" /> <br />

				<label for="confirmationPassword">確認用パスワード(パスワードを再入力)</label><br />
				<input name=confirmationPassword type="password" /> <br />

				<label for="name">ユーザー名(10文字以下)</label><br />
				<input name="name" value="${user.name}" /> <br />

				<label for="branchId">支社</label><br />
				<select name="branchId">
					<c:forEach items="${branches}" var="branch">
						<c:choose>
							<c:when test="${branch.branchId == user.branchId}">
								<option value= "${branch.branchId}" selected>
									<c:out value="${branch.branchName}"></c:out>
								</option>
							</c:when>
							<c:otherwise>
								<option value= "${branch.branchId}">
									<c:out value="${branch.branchName}"></c:out>
								</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select><br />

				<label for="departmentId">部署</label><br />
				<select name="departmentId">
					<c:forEach items="${departments}" var="department">
						<c:choose>
							<c:when test="${department.departmentId == user.departmentId}}">
								<option value= "${department.departmentId}" selected>
									<c:out value="${department.departmentName}"></c:out>
								</option>
							</c:when>
							<c:otherwise>
								<option value= "${department.departmentId}">
									<c:out value="${department.departmentName}"></c:out>
								</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select><br />

				<input type="submit" value="登録" /> <br />
			</form>

			<div class="copyright">Copyright(c)Taniguchi Takaya</div>
		</div>
	</body>
</html>