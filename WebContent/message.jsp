<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿画面</title>
	</head>

	<body>
		<div class="main-contents">
			<div class="header">
				<a href="./">ホーム</a><br />
			</div>

			<c:if test="${not empty errorMessages}">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" /></li>
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<div class="messages">
				<form action="message" method="post"><br />
					<label for="title">件名 </label><br />
					<input name="title" value="${message.title}" /><br />

					<label for="category">カテゴリー</label><br />
					<input name="category" value="${message.category }" /><br />

					<label for="text">本文</label><br />
					<textarea name="text" rows="5" cols="100" ><c:out value ="${message.text}"/></textarea>
					<input type="submit" value="投稿" class="button" /> <br /> <br />
				</form>
			</div>
		<div class="copyright">Copyright(c)Taniguchi Takaya</div>
	</div>
</body>
</html>