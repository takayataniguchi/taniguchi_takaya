<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム画面</title>

		<script type="text/javascript">
			function deleteDialog(string){
				if(window.confirm(string + 'を本当に削除してよろしいですか？')){
					return true;
				}
			return false;
			}
		</script>
	</head>
	<body>
	<div class="main-contents">
		<div class="header">
			<a href="message" class="link">新規投稿</a>
			<c:if test="${loginUser.departmentId == 1}">
				<a href="management" class="link">ユーザー管理</a>
			</c:if>
			<a href="logout" class="link">ログアウト</a>
		</div>

		<div class="comment-error">
			<c:if test="${not empty errorMessages}">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessages">
							<li><c:out value="${errorMessages}" />
						</c:forEach>
					</ul>
				</div>
			</c:if>
			<c:remove var="errorMessages" scope="session" />
		</div>

		<div class="profile">
			<div class="name">名前：<c:out value="${loginUser.name}" /></div>
			<div class="account">アカウント：@<c:out value="${loginUser.account}" /></div>
			<br />
		</div>

		<div class="Messages-filtering">
			<form action="./" method="get"><br />
				<label for="date">日付</label>
					<input type="date" name="startDate" value="${startDate}" />～
					<input type="date" name="endDate" value="${endDate}" /><br />

				<label for="category">カテゴリー</label>
					<input name="category" value="${category}" /> <br />

				<input type="submit" value="絞り込み" /> <br />
			</form>
		</div>

	    <div class="show-messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<span class="userName">
						<c:out value="${message.name}" />
					</span><br />

					<span class="title">
						<c:out value="${message.title}" />
					</span><br />

					<span class="category">
						<c:out value="${message.category}" />
					</span><br />

					<span class="text">
					<pre><c:out value="${message.text}" /></pre>
					</span><br />

					<span class="date">
					 	<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
					</span><br />

					<div class="comment">
						<c:forEach items="${comments}" var="comment">
							<c:if test="${message.id == comment.messageId}">

								<span class="userName">
									<c:out value="${comment.userName}" />
								</span><br />

								<span class="text">
								<pre><c:out value="${comment.text}" /></pre>
								</span><br />

								<span class="date">
									<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
								</span><br />

								<div class="comment-delete">
									<c:if test="${comment.userId == loginUser.userId}">
										<form action="deleteComment" method="post">
											<span class="comment-delete">
												<input type="hidden" name="commentId" value="${comment.id }">
												<input type="submit" value="コメント削除" onClick="return deleteDialog('コメント')">
											</span>
										</form>
									</c:if>
								</div><br />
							</c:if>
						</c:forEach>
					</div>

					<div class="message-delete">
						<c:if test="${message.userId == loginUser.userId}">
							<form action="deleteMessage" method="post">
								<span class="message-delete">
									<input type="hidden" name="messageId" value="${message.id }">
									<input type="submit" value="投稿削除" onClick="return deleteDialog('投稿')">
								</span>
							</form>
						</c:if>
					</div><br />

					<div class="comment-area">
						<form action="comment" method="post">
								コメント入力欄(500文字まで) <br />
								<textarea name="comment" cols="45" rows="5" class="comment-box"></textarea>
								<br />
							<input type="hidden" name="messageId" value="${message.id}">
							<input type="submit" value="投稿" class="button">
						</form>
					</div>
				</div>
			</c:forEach>
		</div>
</div>
	<div class="copyright">Copyright(c)Taniguchi Takaya</div>
</body>
</html>