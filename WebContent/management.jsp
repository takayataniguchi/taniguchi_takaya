<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理画面</title>

		<script type="text/javascript">
			function switchStatusConformationMessage(string){
				if(window.confirm('このユーザーを本当に' + string + 'させてよろしいですか？')){
					return true;
				}
			return false;
			}
		</script>
	</head>
	<body>
	<div class="main-contents">
		<div class="header">
			<a href="./" class="link">ホーム</a>
			<a href="signup" class="link">ユーザー登録</a>
		</div>

	    <div class="user-management">
	    	<c:forEach items="${userBranchDepartments}" var="userBranchDepartment">
				<ul>
					<li>アカウント：<c:out value="${userBranchDepartment.account}"/>
					<li>名前：
					<c:out value="${userBranchDepartment.name}"/>
					<li>支社：
					<c:out value="${userBranchDepartment.branchName}"/>
					<li>部署：
					<c:out value="${userBranchDepartment.departmentName}"/>
					<li>ユーザー停止状態：
					<form action="stop" method="post" style="display:inline">
						<c:choose>
							<c:when test="${userBranchDepartment.isStoped ==1}">
								停止中
								<c:if test="${loginUser.userId != userBranchDepartment.userId}">
									（
									<input type="hidden" value="${userBranchDepartment.userId}" name="userId">
									<input type="hidden" value="0" name="isStoped">
									<input type="submit" value="復活" onClick="return switchStatusConformationMessage('復活')">
									）
								</c:if>
							</c:when>

							<c:otherwise>
								非停止
								<c:if test="${loginUser.userId != userBranchDepartment.userId}">
									（
									<input type="hidden" value="${userBranchDepartment.userId}" name="userId">
									<input type="hidden" value="1" name="isStoped">
									<input type="submit" value="停止" onClick="return switchStatusConformationMessage('停止')">
									）
								</c:if>
							</c:otherwise>
						</c:choose>
					</form>
				</ul>

				<form action="setting" method="get">
					<input type="hidden" value="${userBranchDepartment.userId}" name="userId">
					<input type="submit" value="ユーザー編集">
				</form>

			</c:forEach>
		</div>
	</div>

	<div class="copyright">Copyright(c)Taniguchi Takaya</div>
	</body>
</html>