<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー編集画面</title>
	</head>
	<body>
		<div class="main-contents">

			<a href= "management" class="link">ユーザー管理</a><br /> <br />
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }"></c:out>
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<form action="setting" method="post">
				<input name="userId" value="${editUser.userId}" type="hidden"/>

				<label for="account">アカウント(半角英数字の6文字以上20文字以下)</label><br />
				<input name="account" value="${editUser.account}" /><br />

				<label for="password">パスワード(記号を含む全ての半角文字で6文字以上20文字以下)</label><br />
				<input name="password" type="password" /> <br />

				<label for="confirmationPassword">確認用パスワード(パスワードを再入力)</label><br />
				<input name=confirmationPassword type="password" /> <br />

				<label for="name">名前(10文字以下)</label><br />
				<input name="name" value="${editUser.name}" /> <br />

				<label for="branchId">支社</label><br />
				<select name="branchId">
					<c:forEach items="${editDisplayBranches}" var="branch">
						<c:choose>
							<c:when test="${branch.branchId == editUser.branchId}">
								<option value= "${branch.branchId}" selected>
									<c:out value="${branch.branchName}"></c:out>
								</option>
							</c:when>
							<c:otherwise>
								<c:if test="${loginUser.userId != editUser.userId}">
								<option value= "${branch.branchId}">
									<c:out value="${branch.branchName}"></c:out>
								</option>
								</c:if>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select><br />

				<label for="departmentId">部署</label><br />
				<select name="departmentId">
					<c:forEach items="${editDisplayDepartments}" var="department">
						<c:choose>
							<c:when test="${department.departmentId == editUser.departmentId}">
								<option value= "${department.departmentId}" selected>
									<c:out value="${department.departmentName}"></c:out>
								</option>
							</c:when>
							<c:otherwise>
								<c:if test="${loginUser.userId != editUser.userId}">
								<option value= "${department.departmentId}">
									<c:out value="${department.departmentName}"></c:out>
								</option>
								</c:if>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select><br />

				<input type="submit" value="更新" /> <br />
			</form>

			<div class="copyright">Copyright(c)Taniguchi Takaya</div>
		</div>
	</body>
</html>